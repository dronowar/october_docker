## Установка
```
docker-compose up -d --build
```
- В терминале перейти в контейнер PHP:
```
docker exec -it oc_php bash
```
- и выполнить команды установки October:
```
git clone https://github.com/octobercms/october.git .
composer require --no-update predis/predis
composer install
php artisan october:env
скопировать с заменой файл env.example в папку src и переименовать в .env
php artisan october:up
php artisan october:passwd admin admin
```

- в host прописать 127.0.0.1 october.local
- сайт http://october.local
- PGAdmin http://october.local:5555  
login: admin@admin.com  
password: admin