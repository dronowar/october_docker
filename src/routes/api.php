<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PassportAuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PushController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [PassportAuthController::class, 'register']);
Route::post('login', [PassportAuthController::class, 'login']);

Route::middleware('auth:api')->group(function () {

    Route::resource('/notification',NotificationController::class);
});
Route::post('/registers', [AuthController::class,'register']);
Route::post('/logins',  [AuthController::class,'login']);
Route::post('/logouts',  [AuthController::class,'logout']);
// Route::resource('/client','ClientController');
// Route::resource('/client',ClientController::class);
Route::post('/client',[ClientController::class, 'index']);
Route::post('/clients',[UserController::class, 'store']);
Route::resource('/pushs',PushController::class);

