<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Http\Controllers\NotificationController;
use App\Models\Notification;
use GrahamCampbell\ResultType\Success;
use Illuminate\Support\Facades\View;

class NotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
  

    protected $array = [];
    protected $title;
    protected $body;
    protected $click;
    protected $url_mage;
   
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($array, $title, $body, $click , $url_mage)
    {

        $this->array = $array;
        $this->title = $title;
        $this->body = $body;
        $this->click  = $click;
        $this->url_mage = $url_mage;
     
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
             
                $arr = array('notification' => array("title"=>$this->title ,"body"=>$this->body ,"icon"=>$this->url_mage,"click_action"=> $this->click),   "registration_ids"=> $this->array);
                $a = json_encode($arr);
                
                
                    $curl = curl_init();
            
                    curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS =>$a,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization:  key=AAAA6qGgnLc:APA91bE55BLSD90PrK2GnZrl4bspyNiyB78csBfEsEs7OF5z9CmNOZDcKe9zxXv0aqKDU9O5O9pxvsGQz1NCKCWIrux09bnF1QR8m6qTezLbdYTajdQFCCmzH5V3QvAS6aPXIgFyOEbz',
                        'Content-Type: application/json'
                    ),
                    ));
            
                    $response = curl_exec($curl);
                    
                    curl_close($curl);
                 
                    return $response
                  

    }
    

           
}
