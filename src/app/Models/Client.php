<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
class Client extends Authenticatable
{
    use HasFactory, HasApiTokens;
    protected $fillable = [
        'name',
        'key',
        'user_id',
        'password'
        
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function pushes()
    {
        return $this->hasMany(Push::class);
    }
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
   
}
