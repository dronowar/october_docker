<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;

class PassportAuthController extends Controller
{
    /**
     * Registration
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4',
          
            'key' => 'required|min:8',
            'user_id' => 'required',
        ]);
 
        $user = Client::create([
            'name' => $request->name,
            'password' => bcrypt(Str::random(40)),
            'user_id'=>$request->user_id,
            'key'=>$request->key,
        ]);
       
        $token = $user->createToken('LaravelAuthApp')->accessToken;
 
        return response()->json(['token' => $token], 200);
    }
 
    /**
     * Login
     */
    public function login(Request $request)
    {
        $data = [
            'name' => $request->name,
            'password' => $request->password,
              'user_id'=>$request->user_id,
            'key'=>$request->key,
        ];
 
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }   
}