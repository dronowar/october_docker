<?php

namespace App\Http\Controllers;

use App\Jobs\NotificationJob;
use App\Models\Notification;
use App\Models\Push;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PushController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.index');

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'click' => 'required'
        ]);
  
        $url_mage = '';
        $push = new Push();
        $push->title = $request->title;
        $push->body = $request->body;
        $push->click = $request->click;
        $push->client_id = $request->client_id;
        if ($request->file('icon')){
            $path = Storage::putFile('public', $request->file('icon'));
            $url = Storage::url($path);
            $push->icon = $url;
            $url_mage = $url;
        }
        $push->save();
      

        Notification::where('client_id', '=',$request->client_id) ->chunk(1, function ($items) use ($request,$url_mage ) {
            $array=[];
            $title = $request->title;
            $body = $request->body;
            $click = $request->click;

                foreach($items as $item){
                 
                    array_push( $array,$item->notification_id);
                }
               
                NotificationJob::dispatch($array, $title,$body, $click, $url_mage)->delay(now()->addMinutes(0.1));
           
        },);

     
 
   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Push  $push
     * @return \Illuminate\Http\Response
     */
    public function show(Push $push)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Push  $push
     * @return \Illuminate\Http\Response
     */
    public function edit(Push $push)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Push  $push
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Push $push)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Push  $push
     * @return \Illuminate\Http\Response
     */
    public function destroy(Push $push)
    {
        //
    }
}
