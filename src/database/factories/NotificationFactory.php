<?php

namespace Database\Factories;

use App\Models\Notification;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'client_id'=>rand(1,9),
            'notification_id' => 'FP8d2rY-KKf4CCSAdsBMNtq0DhgcH2AWplqvbLAqrbjYHeWzCR9ZmKadaKwct3zXumxRiW-9APxQk1ePST8YwABL57FcR93HzVAG7vLiGCrJl2tbw6jAzJ'

        ];
    }
}
