<?php

namespace Database\Factories;

use App\Models\Push;
use Illuminate\Database\Eloquent\Factories\Factory;

class PushFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Push::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'client_id'=>rand(1,9),
            'title'=>$this->faker->realText(rand(10,50)),
            'body'=>$this->faker->realText(rand(30,150)),
            'icon'=>'https://ocktula.ru/themes/images/logo.svg',
            'click'=>$this->faker->url
        ];
    }
}
