import { createApp } from 'vue'
import App from './App.vue'
import store from "@/vuex/store"
import router from './router'
import '../node_modules/tailwindcss/dist/base.min.css'
import '../node_modules/tailwindcss/dist/components.min.css'
import '../node_modules/tailwindcss/dist/utilities.min.css'
import '../node_modules/tailwindcss/dist/tailwind.min.css'
import '../node_modules/tailwindcss/dist/tailwind-dark.min.css'


createApp(App).use(router).use(store).mount('#app')

// new Vue({
//     el: '#app',
//     store
//   })