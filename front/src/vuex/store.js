import { createStore } from "vuex" 
import axios from 'axios'
import Auth from "../helpers/Auth"
import router from '../router'

const store = createStore({
    state: {
       
        nav: [
            { path: "/", title: "Home", auth: "both" },
            { path: "/upload_file", title: "File upload", auth: true },
            { path: "/browse_files", title: "Browse files", auth: true },
            { path: "/carousel", title: "Carousel", auth: true },
            { path: "/login", title: "Login", auth: false },
            { path: "/Register", title: "Register", auth: false }
        ],
        clients:[],
        Auth: {
            login: false,
            user_id: null,
            api_token: null,
            name: null,
     
        }
       

    },
     mutations: {
       
        SET_PUSHES_TO_STATE: (state, clients) => {
            state.clients = clients
          

        },
        SET_USER_TO_STATE: (state, response) => {
            state.users = response
          

        },
        Login(state) {
            state.Auth.user_id = localStorage.getItem("user_id");
            state.Auth.name = localStorage.getItem("name");
            state.Auth.api_token = localStorage.getItem("api_token");
            
            state.Auth.login =
                state.Auth.user_id !== null && state.Auth.name !=null && state.Auth.api_token!=null
               
        },
        SET_CLIENTS(state, response){
            state.clients = response.data.data[0].clients
           
        }

    },
    actions: {
        GET_PUSHES_FROM_API({ commit }) {
            return axios('http://october.local/api/client', {
                    method: "GET"
                })
                .then((clients) => {
                    commit('SET_PUSHES_TO_STATE', clients.data.data);
                    return console.log(clients.data.data);
                })
                .catch((error) => {
                    // console.log(error)
                    return error;
                })
        },
      async  ADD_USER_FORM_LOGIN({}, user) {

            return await axios.post('http://october.local/api/logins', user).then((response => {
                Auth.login(response.data)
                Auth.init() 
                }))


        },
         ADD_CLIENT_FORM_LOGIN({ commit }) {
            let user = {
                client: this.state.Auth.user_id,
                api_token:this.state.Auth.api_token,
                
            }
            return  axios.post('http://october.local/api/clients', user).then((response => {
                console.log(response.data.success )
                if(response.data.success === 'true'){
                console.log(response.data.data[0].clients )
                    commit('SET_CLIENTS', response);
                }else{
                    router.push("/login");
                   
                }
               
                }))


        },
          CLIENT({}, user) {

            return axios.post('http://october.local/api/logins', user).then((response => {
                console.log(response.data)
                Auth.login(response.data)
                Auth.init()
                
                this.res = response.data.res
                console.log(this.res)

                  
                }))
        },
        REGISTER({}, user){
            return  axios.post('http://october.local/api/registers', user).then(((response => {
                console.log(response.data)
                if (response.data.success){
                    router.push("/login");
                }
                // this.res = response.data.res
                // console.log(this.res)

                  
                })))

        },
        LOGOUT({}, user) {

            return axios.post('http://october.local/api/logout', user).then((response => {
                console.log(response.data)


                  
                }))
        },
      
    },
    getters: {

        PUSHES(state) {
            return state.clients;
        },
        NAV(state) {
            return state.nav;
        },
        CLIENTS(state) {

            return state.clients;
        },
        AUTH(state) {

            return state.Auth.login;
        },

    }



})

export default store