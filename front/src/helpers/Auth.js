import store from "@/vuex/store"
import router from '../router'

export default {
    init() {
        store.commit("Login");
     
    },

    login(data) {
        localStorage.setItem("user_id", data.user_id);
        localStorage.setItem("api_token", data.user.api_token);
        localStorage.setItem("name", data.user.name);
        // localStorage.setItem("photo", data.photo);

        this.init();
    },

    logout() {
        localStorage.removeItem("user_id");
        localStorage.removeItem("api_token");
        localStorage.removeItem("name");
        localStorage.removeItem("photo");

        this.init();
    },

    check() {
        if (!store.state.Auth.login) {
            router.push("/login");
        }
    }
};